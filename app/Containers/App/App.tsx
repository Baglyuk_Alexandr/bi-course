import * as React from 'react';
import {AuthForm} from '../../Components/AuthForm/AuthForm';
import {Organizations} from '../../Components/Organizations/Organizations';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {IActionType} from '../../common';
import {Actions} from '../../Actions/Actions';
import './App.less';
import {Division} from '../../Components/Divisions/Division';
import {Employee} from '../../Components/Employees/Employee';
import {ModalDelOrganization} from '../../Components/ModalDelOrganization/ModalDelOrganization';
import {ModalEditOrganization} from '../../Components/ModalEditOrganization/ModalEditOrganization';
import {ModalAddOrganization} from '../../Components/ModalAddOrganization/ModalAddOrganization';
import {ModalDelDivision} from '../../Components/ModalDelDivision/ModalDelDivision';
import {ModalEditDivision} from '../../Components/ModalEditDivision/ModalEditDivision';
import {ModalAddDivision} from '../../Components/ModalAddDivision/ModalAddDivision';
import {ModalDelEmployee} from '../../Components/ModalDelEmployee/ModalDelEmployee';
import {ModalEditEmployee} from '../../Components/ModalEditEmployee/ModalEditEmployee';
import {ModalAddEmployee} from '../../Components/ModalAddEmployee/ModalAddEmployee';
import NavBar from '../../Components/NavBar/NavBar';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { IStoreAll } from '../../Actions/Models';
import { IStateProps, IDispatch } from '../../Interfaces/Interfaces';

/**
 * Итоговые пропсы компонента
 */
type TProps = IStateProps & IDispatch;

/**
 * Основной класс приложения.
 */
class App extends React.Component<TProps, {}> {
    /**
     * Обработчик выхода из системы. !!!!!! Не передан в форму
     */
    handleLogout = () => this.props.actions.onLogout();

    render() {
        const {loginStatus,waitingForLogin} = this.props;
        return (
            <Router>
                <div className="content">
                    <ModalAddOrganization/>
                    <ModalDelOrganization/>
                    <ModalEditOrganization/>
                    <ModalAddDivision/>
                    <ModalDelDivision/>
                    <ModalEditDivision/>
                    <ModalAddEmployee/>
                    <ModalDelEmployee/>
                    <ModalEditEmployee/>
                    <NavBar
                        waitingForLogin = {waitingForLogin}
                        loginStatus = {loginStatus}
                    />
                    {/* exact - типа точный url путь */}
                    <Switch>
                        <Route path="/" component={AuthForm} exact/>
                        <Route path="/division/:id" component={Division}/>
                        <Route path="/employee/:id" component={Employee}/>
                        <Route path="/organizations" component={Organizations}/>
                    </Switch>
                </div>
            </Router>
        );
    }
}

function mapStateToProps(state: IStoreAll): IStateProps {
    // console.log('type is - ' + typeof state.organizations);
    return {
        loginStatus: state.reducerAuthForm.loginStatus,
        waitingForLogin: state.reducerAuthForm.loading
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    };
}

const connectApp = connect(mapStateToProps, mapDispatchToProps)(App);

export {connectApp as App};
