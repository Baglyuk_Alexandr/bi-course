import {Dispatch} from 'redux';
import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from './Consts';
import {ILoginData} from './Models';

/**
 * Экшены для приложения.
 */
export class Actions {
    constructor(private dispatch: Dispatch<IActionType>) {
    }

    onLogin = (loginData: ILoginData) => {
        this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`});
        const options = {
            method: 'POST',
            body: JSON.stringify({loginData}),
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            },
        };
        fetch('http://127.0.0.1:3002/athorize', options)
            .then(response => {
                if (response.status === 200) {
                    this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`});
                } else {
                    throw 'error';
                }
            })
            .catch(error => {
                this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`, payload: error});
            });
    };

    onLogout = () => {
        const options = {
            method: 'POST',
        };
        fetch('http://127.0.0.1:3002/logout', options)
        .then(response => {
            if (response.status === 200) {
                this.dispatch({type: ActionTypes.LOGOUT});
            } else {
                throw 'error';
            }
        })
        .catch(() => {
            this.dispatch({type: ActionTypes.LOGOUT});
        });
    }

    initOrgData = () => {
        const options = {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            }
        };
        fetch('http://127.0.0.1:3002/organization', options)
            .then((response) => {
                if (!response.ok) {
                    console.log('ОШИБКА');
                    throw new Error(response.statusText); }
                return response.json()
            })
            .then((organizationsList) => {
                this.dispatch({type: `${ActionTypes.ORGANIZATIONS}${AsyncActionTypes.SUCCESS}`,
                                payload:organizationsList})
            })
            // .catch((error) => {
            //     this.dispatch({type: `${ActionTypes.ORGANIZATIONS}${AsyncActionTypes.FAILURE}`})
            // })
    }

    initDivData = (id: any) => {
        const options = {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            }
        };
        fetch(`http://127.0.0.1:3002/division?id=${id}`, options)
            .then((response) => {
                if (!response.ok) {
                    console.log('ОШИБКА');
                    throw new Error(response.statusText); }
                return response.json()
            })
            .then((divisionsList) => {
                if(divisionsList.length === 0) {
                    console.log( 'сделать вывод уведомления пустого списка')
                }
                this.dispatch({type: `${ActionTypes.DIVISION}${AsyncActionTypes.SUCCESS}`,
                                payload:divisionsList})
            })
            // .catch((error) => {
            //     console.log(' ERROR --- ' + error)
            // })
    }

    initEmplData = (id: any) => {
        const options = {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            }
        };
        fetch(`http://127.0.0.1:3002/employee?id=${id}`, options)
            .then((response) => {
                if (!response.ok) {
                    console.log('ОШИБКА');
                    throw new Error(response.statusText); }
                return response.json()
            })
            .then((employeesList) => {
                this.dispatch({type: `${ActionTypes.EMPLOYEE}${AsyncActionTypes.SUCCESS}`,
                                payload:employeesList})
            })
            // .catch((error) => {
            //     console.log(' ERROR --- ' + error)
            // })
    }

    // MODAL WINDOW
    // openModal = () => {
    //     this.dispatch({type: `${ActionTypes.OPEN_MODAL}`});
    // }

    openModalDelOrganization = (id: number) => {
        this.dispatch({type: `${ActionTypes.OPEN_MODAL_DEL_ORGANIZATION}`,
                        payload: id});
    }

    openModalEditOrganization = (id: number, name: string, address: string, INN: number) => {
        const organization = {
            id,name,address,INN
        }
        this.dispatch({type: `${ActionTypes.OPEN_MODAL_EDIT_ORGANIZATION}`,
                        payload: organization});
    }

    openModalAddOrganization = () => {
        this.dispatch({type: `${ActionTypes.OPEN_MODAL_ADD_ORGANIZATION}`,
                        payload: {}});
    }

    openModalDelDivision = (id: number) => {
        this.dispatch({type: `${ActionTypes.OPEN_MODAL_DEL_DIVISION}`,
                        payload: id});
    }

    openModalEditDivision = (id: number, name: string, phone: number) => {
        const division = {
            id,name,phone
        }
        console.log(division)
        this.dispatch({type: `${ActionTypes.OPEN_MODAL_EDIT_DIVISION}`,
                        payload: division});
    }

    openModalAddDivision = () => {
        this.dispatch({type: `${ActionTypes.OPEN_MODAL_ADD_DIVISION}`,
                        payload: {}});
    }

    openModalDelEmployee = (id: number) => {
        this.dispatch({type: `${ActionTypes.OPEN_MODAL_DEL_EMPLOYEE}`,
                        payload: id});
    }

    openModalEditEmployee = (id: number, FIO: string, address: string, position: string) => {
        const employee = {
            id,FIO,address,position
        }
        console.log(employee)
        this.dispatch({type: `${ActionTypes.OPEN_MODAL_EDIT_EMPLOYEE}`,
                        payload: employee});
    }

    openModalAddEmployee = () => {
        this.dispatch({type: `${ActionTypes.OPEN_MODAL_ADD_EMPLOYEE}`,
                        payload: {}});
    }

    modalDelApproveOrganization = (id: number) => {
        console.log(`approve DILETE item with id - ${id}`)
        // const options = {
        //     method: 'POST',
        //     body: JSON.stringify({id}),
        //     headers: {
        //         'Content-Type': 'application/json;charset=utf-8',
        //     },
        // };
        // fetch('http://127.0.0.1:3002/deleteOrganization', options)
        // .then(response => {
        //     if (response.status === 200) {
        //         console.log('Удаление завершено')
        //     }
        // })
    }

    modalEditApproveOrganization = (id: number, name: string, address: string, INN: number) => {
        console.log(`approve EDIT item with id - ${id} name - ${name} address - ${address} INN - ${INN}`)
        // const options = {
        //     method: 'POST',
        //     body: JSON.stringify({id}),
        //     headers: {
        //         'Content-Type': 'application/json;charset=utf-8',
        //     },
        // };
        // fetch('http://127.0.0.1:3002/deleteOrganization', options)
        // .then(response => {
        //     if (response.status === 200) {
        //         console.log('Редактирование завершено')
        //     }
        // })
    }

    modalAddApproveOrganization = (id: number, name: string, address: string, INN: number) => {
        console.log(`approve Add item with id - ${id} name - ${name} address - ${address} INN - ${INN}`)
        // const options = {
        //     method: 'POST',
        //     body: JSON.stringify({id}),
        //     headers: {
        //         'Content-Type': 'application/json;charset=utf-8',
        //     },
        // };
        // fetch('http://127.0.0.1:3002/deleteOrganization', options)
        // .then(response => {
        //     if (response.status === 200) {
        //         console.log('Редактирование завершено')
        //     }
        // })
    }

    modalDelApproveDivision = (id: number) => {
        console.log(`approve DILETE item with id - ${id}`)
        // const options = {
        //     method: 'POST',
        //     body: JSON.stringify({id}),
        //     headers: {
        //         'Content-Type': 'application/json;charset=utf-8',
        //     },
        // };
        // fetch('http://127.0.0.1:3002/deleteOrganization', options)
        // .then(response => {
        //     if (response.status === 200) {
        //         console.log('Удаление завершено')
        //     }
        // })
    }

    modalEditApproveDivision = (id: number, name: string, phone: number) => {
        console.log(`approve EDIT item with id - ${id} name - ${name} phone - ${phone}`)
        // const options = {
        //     method: 'POST',
        //     body: JSON.stringify({id}),
        //     headers: {
        //         'Content-Type': 'application/json;charset=utf-8',
        //     },
        // };
        // fetch('http://127.0.0.1:3002/deleteOrganization', options)
        // .then(response => {
        //     if (response.status === 200) {
        //         console.log('Редактирование завершено')
        //     }
        // })
    }

    modalAddApproveDivision = (id: number, name: string, address: string, INN: number) => {
        console.log(`approve Add item with id - ${id} name - ${name} address - ${address} INN - ${INN}`)
        // const options = {
        //     method: 'POST',
        //     body: JSON.stringify({id}),
        //     headers: {
        //         'Content-Type': 'application/json;charset=utf-8',
        //     },
        // };
        // fetch('http://127.0.0.1:3002/deleteOrganization', options)
        // .then(response => {
        //     if (response.status === 200) {
        //         console.log('Редактирование завершено')
        //     }
        // })
    }

    modalDelApproveEmployee = (id: number) => {
        console.log(`approve DILETE item with id - ${id}`)
        // const options = {
        //     method: 'POST',
        //     body: JSON.stringify({id}),
        //     headers: {
        //         'Content-Type': 'application/json;charset=utf-8',
        //     },
        // };
        // fetch('http://127.0.0.1:3002/deleteOrganization', options)
        // .then(response => {
        //     if (response.status === 200) {
        //         console.log('Удаление завершено')
        //     }
        // })
    }

    modalEditApproveEmployee = (id: number, FIO: string, address: string, vacansiya: string) => {
        console.log(`approve EDIT item with id - ${id} FIO - ${FIO} address - ${address} vacansiya - ${vacansiya}`)
        // const options = {
        //     method: 'POST',
        //     body: JSON.stringify({id}),
        //     headers: {
        //         'Content-Type': 'application/json;charset=utf-8',
        //     },
        // };
        // fetch('http://127.0.0.1:3002/deleteOrganization', options)
        // .then(response => {
        //     if (response.status === 200) {
        //         console.log('Редактирование завершено')
        //     }
        // })
    }

    modalAddApproveEmployee = (id: number, name: string, address: string, INN: number) => {
        console.log(`approve Add item with id - ${id} name - ${name} address - ${address} INN - ${INN}`)
        // const options = {
        //     method: 'POST',
        //     body: JSON.stringify({id}),
        //     headers: {
        //         'Content-Type': 'application/json;charset=utf-8',
        //     },
        // };
        // fetch('http://127.0.0.1:3002/deleteOrganization', options)
        // .then(response => {
        //     if (response.status === 200) {
        //         console.log('Редактирование завершено')
        //     }
        // })
    }

    closeModalDelOrganization = () => {
        this.dispatch({type: `${ActionTypes.CLOSE_MODAL_DEL_ORGANIZATION}`});
    }

    closeModalEditOrganization = () => {
        this.dispatch({type: `${ActionTypes.CLOSE_MODAL_EDIT_ORGANIZATION}`});
    }

    closeModalAddOrganization = () => {
        this.dispatch({type: `${ActionTypes.CLOSE_MODAL_ADD_ORGANIZATION}`});
    }

    closeModalDelDivision = () => {
        this.dispatch({type: `${ActionTypes.CLOSE_MODAL_DEL_DIVISION}`});
    }

    closeModalEditDivision = () => {
        this.dispatch({type: `${ActionTypes.CLOSE_MODAL_EDIT_DIVISION}`});
    }

    closeModalAddDivision = () => {
        this.dispatch({type: `${ActionTypes.CLOSE_MODAL_ADD_DIVISION}`});
    }

    closeModalDelEmployee = () => {
        this.dispatch({type: `${ActionTypes.CLOSE_MODAL_DEL_EMPLOYEE}`});
    }

    closeModalEditEmployee = () => {
        this.dispatch({type: `${ActionTypes.CLOSE_MODAL_EDIT_EMPLOYEE}`});
    }

    closeModalAddEmployee = () => {
        this.dispatch({type: `${ActionTypes.CLOSE_MODAL_ADD_EMPLOYEE}`});
    }
}
