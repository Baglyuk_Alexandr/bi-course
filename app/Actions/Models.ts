import { IStoreStateAuthForm } from "../Reducers/ReducersAuthForm";
import { 
    IStateListOrganizations, 
    IStateListDivision, 
    IStateListEmployee, 
    IStoreStateModalDelOrganization, 
    IStoreStateModalEditOrganization,
    IStoreStateModalAddOrganization,
    IStoreStateModalDelDivision, 
    IStoreStateModalEditDivision,
    IStoreStateModalAddDivision,
    IStoreStateModalDelEmployee, 
    IStoreStateModalEditEmployee,
    IStoreStateModalAddEmployee,
} from "../Interfaces/Interfaces";

/**
 * Модель данных для авторизации.
 * @prop {string} login Имя пользователя.
 * @prop {string} password Пароль пользователя.
 */
export interface ILoginData {
    login: string;
    password: string;
}

export interface IStoreAll {
    reducerAuthForm: IStoreStateAuthForm
    reducerOrganizations: IStateListOrganizations
    reducerDivision: IStateListDivision
    reducerEmployee: IStateListEmployee
    reducerModalDelOrganization: IStoreStateModalDelOrganization
    reducerModalEditOrganization: IStoreStateModalEditOrganization
    reducerModalAddOrganization: IStoreStateModalAddOrganization
    reducerModalDelDivision: IStoreStateModalDelDivision
    reducerModalEditDivision: IStoreStateModalEditDivision
    reducerModalAddDivision: IStoreStateModalAddDivision
    reducerModalDelEmployee: IStoreStateModalDelEmployee
    reducerModalEditEmployee: IStoreStateModalEditEmployee
    reducerModalAddEmployee: IStoreStateModalAddEmployee
}
