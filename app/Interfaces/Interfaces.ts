import { Actions } from "../Actions/Actions";

export interface IDispatch{
    actions: Actions
}

export interface IStateDivision {
    id: number;
    id_organization: number;
    name: string;
    phone: number
}

export interface IStateListDivision {
    division: IStateDivision[];
}

export interface IStateEmployee {
    id: number;
    id_division: number;
    FIO: string;
    address: string;
    position: string;
}

export interface IStateListEmployee {
    employee: IStateEmployee[];
}

export interface IStateOrganization {
    id: number;
    name: string;
    address: string;
    INN: number
}

export interface IStateOrganizations {
    loginStatus: boolean;
    organizations: IStateOrganization[];
}

export interface IStateListOrganizations {
    organizations: IStateOrganization[];
}

export interface IStateProps{
    loginStatus: boolean;
    waitingForLogin: boolean;
}

interface IMatch {
    params: {id: string};
}
// для работы с match (url)
export interface IPropsMatch {
match: IMatch;
}

// MODAL WINDOW
export interface IStoreStateModalDelOrganization {
    isOpenDelOrganization: boolean;
    id: number;
}

export interface IStoreStateModalEditOrganization {
    isOpenEditOrganization: boolean;
    organization: any;
}

export interface IStoreStateModalAddOrganization {
    isOpenAddOrganization: boolean;
    organization: any;
}

export interface IStoreStateModalDelDivision {
    isOpenDelDivision: boolean;
    id: number;
}

export interface IStoreStateModalEditDivision {
    isOpenEditDivision: boolean;
    division: any;
}

export interface IStoreStateModalAddDivision {
    isOpenAddDivision: boolean;
    division: any;
}

export interface IStoreStateModalDelEmployee {
    isOpenDelEmployee: boolean;
    id: number;
}

export interface IStoreStateModalEditEmployee {
    isOpenEditEmployee: boolean;
    employee: any;
}

export interface IStoreStateModalAddEmployee {
    isOpenAddEmployee: boolean;
    employee: any;
}