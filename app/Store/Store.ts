import {applyMiddleware, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {ReducersCombine} from '../Reducers/ReducersCombine';

/**
 * Redux хранилище (стор).
 */
const store = createStore(ReducersCombine, composeWithDevTools(applyMiddleware(thunk)));

export {store as appStore};
