import {IActionType} from '../common';
import {ActionTypes} from '../Actions/Consts';
import { IStoreStateModalDelDivision } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStoreStateModalDelDivision {
        return {
            isOpenDelDivision: false,
            id: null
        };
    }
};

export function reducerModalDelDivision(state: IStoreStateModalDelDivision = initialState.state, action: IActionType) {
    switch (action.type) {

        case `${ActionTypes.OPEN_MODAL_DEL_DIVISION}`:
            return {
                isOpenDelDivision: true,
                id: action.payload
            };

        case `${ActionTypes.CLOSE_MODAL_DEL_DIVISION}`:
            return {
                isOpenDelDivision: false
            };
    }
    return state;
}
