import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts';
import { IStateListDivision } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStateListDivision {
        return {
            division:[]
        };
    }
};

export function reducerDivision(state: IStateListDivision = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.DIVISION}${AsyncActionTypes.SUCCESS}`:
            return {
                division: action.payload
            };
    }
    // console.log(state);
    return state;
}
