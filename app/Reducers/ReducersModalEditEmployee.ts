import {IActionType} from '../common';
import {ActionTypes} from '../Actions/Consts';
import { IStoreStateModalEditEmployee } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStoreStateModalEditEmployee {
        return {
            isOpenEditEmployee: false,
            employee: {}
        };
    }
};

export function reducerModalEditEmployee(state: IStoreStateModalEditEmployee = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.OPEN_MODAL_EDIT_EMPLOYEE}`:
            return {
                isOpenEditEmployee: true,
                employee: action.payload
            };

        case `${ActionTypes.CLOSE_MODAL_EDIT_EMPLOYEE}`:
            return {
                isOpenEditEmployee: false,
                employee: state.employee
            };
    }
    return state;
}
