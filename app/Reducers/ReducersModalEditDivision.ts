import {IActionType} from '../common';
import {ActionTypes} from '../Actions/Consts';
import { IStoreStateModalEditDivision } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStoreStateModalEditDivision {
        return {
            isOpenEditDivision: false,
            division: {}
        };
    }
};

export function reducerModalEditDivision(state: IStoreStateModalEditDivision = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.OPEN_MODAL_EDIT_DIVISION}`:
            return {
                isOpenEditDivision: true,
                division: action.payload
            };

        case `${ActionTypes.CLOSE_MODAL_EDIT_DIVISION}`:
            return {
                isOpenEditDivision: false,
                division: state.division
            };
    }
    return state;
}
