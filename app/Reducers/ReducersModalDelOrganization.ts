import {IActionType} from '../common';
import {ActionTypes} from '../Actions/Consts';
import { IStoreStateModalDelOrganization } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStoreStateModalDelOrganization {
        return {
            isOpenDelOrganization: false,
            id: null
        };
    }
};

export function reducerModalDelOrganization(state: IStoreStateModalDelOrganization = initialState.state, action: IActionType) {
    switch (action.type) {

        case `${ActionTypes.OPEN_MODAL_DEL_ORGANIZATION}`:
            return {
                isOpenDelOrganization: true,
                id: action.payload
            };

        case `${ActionTypes.CLOSE_MODAL_DEL_ORGANIZATION}`:
            return {
                isOpenDelOrganization: false
            };
    }
    return state;
}
