import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts';

/**
 * Состояние для Redux хранилища (стора).
 * @prop {boolean} loginStatus Состояние зарегистрированности пользователя.
 * @prop {boolean} loading Ожидание завершения процедуры авторизации (завершение логина).
 */
export interface IStoreStateAuthForm{
    loginStatus: boolean;
    loading: boolean;
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreStateAuthForm {
        return {
            loginStatus: false,
            loading: false,
        };
    }
};

export function reducerAuthForm(state: IStoreStateAuthForm = initialState.state, action: IActionType) {
    switch (action.type) {

        case `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                loading: true,
            };

        case `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`:
            return {
                loginStatus: true,
                loading: false,
            };

        case `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`:
            return {
                loading: false,
                loginStatus: false,
            };

        case ActionTypes.LOGOUT:
            return {
                ...state,
                loginStatus: false,
            };
    }
    return state;
}
