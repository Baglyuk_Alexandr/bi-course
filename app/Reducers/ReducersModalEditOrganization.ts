import {IActionType} from '../common';
import {ActionTypes} from '../Actions/Consts';
import { IStoreStateModalEditOrganization } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStoreStateModalEditOrganization {
        return {
            isOpenEditOrganization: false,
            organization: {}
        };
    }
};

export function reducerModalEditOrganization(state: IStoreStateModalEditOrganization = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.OPEN_MODAL_EDIT_ORGANIZATION}`:
            return {
                isOpenEditOrganization: true,
                organization: action.payload
            };

        case `${ActionTypes.CLOSE_MODAL_EDIT_ORGANIZATION}`:
            return {
                isOpenEditOrganization: false,
                organization: state.organization
            };
    }
    return state;
}
