import {IActionType} from '../common';
import {ActionTypes} from '../Actions/Consts';
import { IStoreStateModalEdit } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStoreStateModalEdit {
        return {
            isOpenEdit: false,
            organization: {}
        };
    }
};

export function reducerModalEdit(state: IStoreStateModalEdit = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.OPEN_MODAL_EDIT}`:
            return {
                isOpenEdit: true,
                organization: action.payload
            };

        case `${ActionTypes.CLOSE_MODAL_EDIT}`:
            return {
                isOpenEdit: false
            };
    }
    return state;
}
