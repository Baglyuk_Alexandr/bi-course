import {IActionType} from '../common';
import {ActionTypes} from '../Actions/Consts';
import { IStoreStateModalAddDivision } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStoreStateModalAddDivision {
        return {
            isOpenAddDivision: false,
            division: {}
        };
    }
};

export function reducerModalAddDivision(state: IStoreStateModalAddDivision = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.OPEN_MODAL_ADD_DIVISION}`:
            return {
                isOpenAddDivision: true,
                division: action.payload
            };

        case `${ActionTypes.CLOSE_MODAL_ADD_DIVISION}`:
            return {
                isOpenAddDivision: false,
                division: state.division
            };
    }
    return state;
}
