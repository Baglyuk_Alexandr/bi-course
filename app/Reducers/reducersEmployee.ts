import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts';
import { IStateListEmployee } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStateListEmployee {
        return {
            employee:[]
        };
    }
};

export function reducerEmployee(state: IStateListEmployee = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.EMPLOYEE}${AsyncActionTypes.SUCCESS}`:
            return {
                employee: action.payload
            };
    }
    // console.log(state);
    return state;
}
