import {IActionType} from '../common';
import {ActionTypes} from '../Actions/Consts';
import { IStoreStateModalAddEmployee } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStoreStateModalAddEmployee {
        return {
            isOpenAddEmployee: false,
            employee: {}
        };
    }
};

export function reducerModalAddEmployee(state: IStoreStateModalAddEmployee = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.OPEN_MODAL_ADD_EMPLOYEE}`:
            return {
                isOpenAddEmployee: true,
                employee: action.payload
            };

        case `${ActionTypes.CLOSE_MODAL_ADD_EMPLOYEE}`:
            return {
                isOpenAddEmployee: false,
                employee: state.employee
            };
    }
    return state;
}
