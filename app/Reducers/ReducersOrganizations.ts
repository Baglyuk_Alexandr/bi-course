import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts';
import { IStateListOrganizations } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStateListOrganizations {
        return {
            organizations: []
        };
    }
};

export function reducerOrganizations(state: IStateListOrganizations = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.ORGANIZATIONS}${AsyncActionTypes.SUCCESS}`:
            return {
                organizations: action.payload
            };
    }
    return state;
}
