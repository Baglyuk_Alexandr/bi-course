import { combineReducers } from 'redux'
import { reducerAuthForm } from './ReducersAuthForm'
import { reducerOrganizations } from './ReducersOrganizations'
import { reducerDivision } from './reducersDivision'
import { reducerEmployee } from './reducersEmployee'
import { reducerModalDelOrganization } from './ReducersModalDelOrganization'
import { reducerModalEditOrganization } from './ReducersModalEditOrganization'
import { reducerModalAddOrganization } from './ReducersModalAddOrganization'
import { reducerModalDelDivision } from './ReducersModalDelDivision'
import { reducerModalEditDivision } from './ReducersModalEditDivision'
import { reducerModalAddDivision } from './ReducersModalAddDivision'
import { reducerModalDelEmployee } from './ReducersModalDelEmployee'
import { reducerModalEditEmployee } from './ReducersModalEditEmployee'
import { reducerModalAddEmployee } from './ReducersModalAddEmployee'

export const ReducersCombine = combineReducers({
    reducerAuthForm,
    reducerOrganizations,
    reducerDivision,
    reducerEmployee,
    reducerModalDelOrganization,
    reducerModalEditOrganization,
    reducerModalAddOrganization,
    reducerModalDelDivision,
    reducerModalEditDivision,
    reducerModalAddDivision,
    reducerModalDelEmployee,
    reducerModalEditEmployee,
    reducerModalAddEmployee,
})
