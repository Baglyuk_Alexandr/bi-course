import {IActionType} from '../common';
import {ActionTypes} from '../Actions/Consts';
import { IStoreStateModalAddOrganization } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStoreStateModalAddOrganization {
        return {
            isOpenAddOrganization: false,
            organization: {}
        };
    }
};

export function reducerModalAddOrganization(state: IStoreStateModalAddOrganization = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.OPEN_MODAL_ADD_ORGANIZATION}`:
            return {
                isOpenAddOrganization: true,
                organization: action.payload
            };

        case `${ActionTypes.CLOSE_MODAL_ADD_ORGANIZATION}`:
            return {
                isOpenAddOrganization: false,
                organization: state.organization
            };
    }
    return state;
}
