import {IActionType} from '../common';
import {ActionTypes} from '../Actions/Consts';
import { IStoreStateModalDel } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStoreStateModalDel {
        return {
            isOpenDel: false,
            id: null
        };
    }
};

export function reducerModalDel(state: IStoreStateModalDel = initialState.state, action: IActionType) {
    switch (action.type) {

        case `${ActionTypes.OPEN_MODAL_DEL}`:
            return {
                isOpenDel: true,
                id: action.payload
            };

        case `${ActionTypes.CLOSE_MODAL_DEL}`:
            return {
                isOpenDel: false
            };
    }
    return state;
}
