import {IActionType} from '../common';
import {ActionTypes} from '../Actions/Consts';
import { IStoreStateModalDelEmployee } from '../Interfaces/Interfaces';

const initialState = {
    get state(): IStoreStateModalDelEmployee {
        return {
            isOpenDelEmployee: false,
            id: null
        };
    }
};

export function reducerModalDelEmployee(state: IStoreStateModalDelEmployee = initialState.state, action: IActionType) {
    switch (action.type) {

        case `${ActionTypes.OPEN_MODAL_DEL_EMPLOYEE}`:
            return {
                isOpenDelEmployee: true,
                id: action.payload
            };

        case `${ActionTypes.CLOSE_MODAL_DEL_EMPLOYEE}`:
            return {
                isOpenDelEmployee: false
            };
    }
    return state;
}
