import * as React from 'react';
import { Actions } from '../../Actions/Actions';
import { IStoreAll } from '../../Actions/Models';
import { Dispatch } from 'redux';
import { IActionType } from '../../common';
import { IDispatch, IStoreStateModalAddOrganization } from '../../Interfaces/Interfaces';
import { connect } from 'react-redux';
import './ModalAddOrganization.less';

type IModaAddOrganization = IStoreStateModalAddOrganization & IDispatch & any

class ModalAddOrganization extends React.Component<IModaAddOrganization> {

    state = {
        id: 100,
        name: '',
        address: '',
        INN: 0
    }

    idChange = (e: any) => {
        this.setState({
            id: e.target.value
        });
    }

    nameChange = (e: any) => {
        this.setState({
            name: e.target.value
        });
    }

    addressChange = (e: any) => {
        this.setState({
            address: e.target.value
        });
    }

    innChange = (e: any) => {
        this.setState({
            INN: e.target.value
        });
    }

    modalAddApproveOrganization = (e: any, id: number, name: string, address: string, INN: number)  =>  {
        e.preventDefault();
        this.props.actions.modalAddApproveOrganization(id, name, address, INN);
    }

    render() {
        const {isOpenAddOrganization} = this.props;

        if(!isOpenAddOrganization) return null;
        return (
            <div className="modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Добавление записи</h5>
                            <button onClick={this.props.actions.closeModalAddOrganization} type="button" className="close">
                            <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="item-add-form d-flex flex-column">
                                <label>
                                    ID:
                                    <input type="text"
                                        placeholder='Введите ID'
                                        className="form-control"
                                        required
                                        onChange={this.idChange}
                                    />
                                </label>
                                <label>
                                    Name:
                                    <input type="text"
                                        placeholder='Введите название'
                                        className="form-control"
                                        required
                                        onChange={this.nameChange}
                                    />
                                </label>
                                <label>
                                    Address:
                                    <input type="text"
                                        placeholder='Введите адрес'
                                        className="form-control"
                                        required
                                        onChange={this.addressChange}
                                    />
                                </label>
                                <label>
                                    INN:
                                    <input type="text"
                                        placeholder='Введите ИНН'
                                        className="form-control"
                                        required
                                        onChange={this.innChange}
                                    />
                                </label>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary"
                                onClick={(e: any) =>
                                    this.modalAddApproveOrganization(
                                        e,
                                        this.state.id,
                                        this.state.name,
                                        this.state.address,
                                        this.state.INN
                                    )
                                }>
                                Подтвердить
                            </button>
                            <button
                                onClick={this.props.actions.closeModalAddOrganization} type="button"
                                className="btn btn-secondary">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStoreStateModalAddOrganization {
    return {
        isOpenAddOrganization: state.reducerModalAddOrganization.isOpenAddOrganization,
        organization: state.reducerModalAddOrganization.organization
    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    }
}

const connectModalAddOrganization = connect(mapStateToProps,mapDispatchToProps)(ModalAddOrganization);
export {connectModalAddOrganization as ModalAddOrganization}
