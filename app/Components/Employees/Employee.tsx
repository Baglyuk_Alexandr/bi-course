import * as React from 'react';
import { Dispatch } from 'redux';
import './Employees.less';
import { IActionType } from '../../common';
import { connect } from 'react-redux';
import { Actions } from '../../Actions/Actions';
import { IStoreAll } from '../../Actions/Models';
import { IDispatch, IPropsMatch, IStateEmployee, IStateListEmployee } from '../../Interfaces/Interfaces';
import { Link } from 'react-router-dom';

type IDiv = IStateListEmployee & IDispatch & IPropsMatch

class Employee extends React.Component<IDiv> {

    componentDidMount() {
        const id = this.props.match.params.id; // id from url
        this.props.actions.initEmplData(id);
    }

    openModalDelEmployee = (e: any, id: number)  =>  {
        e.preventDefault();
        this.props.actions.openModalDelEmployee(id);
    }

    openModalEditEmployee = (e: any, id: number, FIO: string, address: string, position: string)  =>  {
        e.preventDefault();
        this.props.actions.openModalEditEmployee(id, FIO, address, position);
    }

    render() {
        const employee = this.props.employee.map((item: IStateEmployee) => {
            const {id, FIO, address, position} = item;
            return (
                <div className="d-tr" key={id}>
                    <div className="d-td">{id}</div>
                    <div className="d-td">{FIO}</div>
                    <div className="d-td">{address}</div>
                    <div className="d-td">{position}</div>
                    <div className="d-td">
                        <div className="b-container">
                            <button
                                onClick={(e: any) => this.openModalEditEmployee(e, id, FIO, address, position)}>
                                    Изменить
                            </button>
                            <button
                                onClick={(e: any) => this.openModalDelEmployee(e, id)}>
                                    Удалить
                            </button>
                        </div>
                    </div>
                </div>
            )
        })
        return (
            <div className="d-container">
                <h1>Работники</h1>
                <div className="innerNav">
                    <Link className="f-right"to={`/division/${this.props.match.params.id}`}>← Подразделения</Link>
                    <Link className="f-right mr-10"to="/organizations">← Организации</Link>
                </div>
                <div>
                    <button className="addItem"
                        onClick={this.props.actions.openModalAddEmployee}>
                            Добавить запись
                    </button>
                </div>
                <div className="d-table">
                    <div className="d-tr">
                        <div className="d-td">ID</div>
                        <div className="d-td">FIO</div>
                        <div className="d-td">Adres</div>
                        <div className="d-td">Vacansiya</div>
                        <div className="d-td">Controls</div>
                    </div>
                    {employee.length > 0? <>{employee}</>:<h1>Список пуст</h1>}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStateListEmployee {
    return {
        employee: state.reducerEmployee.employee
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    };
}

const connectEmployee = connect(mapStateToProps, mapDispatchToProps)(Employee);

export {connectEmployee as Employee};
