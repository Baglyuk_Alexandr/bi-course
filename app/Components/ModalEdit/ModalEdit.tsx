import * as React from 'react';
import { Actions } from '../../Actions/Actions';
import { IStoreAll } from '../../Actions/Models';
import { Dispatch } from 'redux';
import { IActionType } from '../../common';
import { IDispatch, IStoreStateModalEdit } from '../../Interfaces/Interfaces';
import { connect } from 'react-redux';
import './ModalEdit.less';

type IModalDel = IStoreStateModalEdit & IDispatch & any

class ModalEdit extends React.Component<IModalDel> {

    modalEditApprove = (e: any, id: number, name: string, address: string, INN: number)  =>  {
        e.preventDefault();
        this.props.actions.modalEditApprove(id, name, address, INN);
    }

    render() {
        const {isOpenEdit} = this.props;

        const {id, name, address, INN} = this.props.organization;

        if(!isOpenEdit) return null;
        return (
            <div className="modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Редактирование записи</h5>
                            <button onClick={this.props.actions.closeModalEdit} type="button" className="close">
                            <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="item-add-form d-flex flex-column">
                                <label>
                                    Name:
                                    <input type="text"
                                        placeholder={name}
                                        className="form-control"
                                        // required
                                        />
                                </label>
                                <label>
                                    Address:
                                    <input type="password"
                                        placeholder={address}
                                        className="form-control"
                                        // required
                                        />
                                </label>
                                <label>
                                    INN:
                                    <input type="password"
                                        placeholder={INN}
                                        className="form-control"
                                        // required=
                                        />
                                </label>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary"
                                onClick={(e: any) => this.modalEditApprove(e, id, name, address, INN)}>
                                Подтвердить
                            </button>
                            <button
                                onClick={this.props.actions.closeModalEdit} type="button"
                                className="btn btn-secondary">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStoreStateModalEdit {
    return {
        isOpenEdit: state.reducerModalEdit.isOpenEdit,
        organization: state.reducerModalEdit.organization
    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    }
}

const connectModalEdit = connect(mapStateToProps,mapDispatchToProps)(ModalEdit);
export {connectModalEdit as ModalEdit}
