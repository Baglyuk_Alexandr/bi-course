import * as React from 'react';
import { Actions } from '../../Actions/Actions';
import { IStoreAll } from '../../Actions/Models';
import { Dispatch } from 'redux';
import { IActionType } from '../../common';
import { IDispatch, IStoreStateModalEditDivision } from '../../Interfaces/Interfaces';
import { connect } from 'react-redux';
import './ModalEditDivision.less';

type IModalEditDivision = IStoreStateModalEditDivision & IDispatch & any

class ModalEditDivision extends React.Component<IModalEditDivision> {

    state = {
        name: '',
        phone: 0,
    }

    nameChange = (e: any) => {
        this.setState({
            name: e.target.value
        });
    }

    phoneChange = (e: any) => {
        this.setState({
            phone: e.target.value
        });
    }

    modalEditApproveDivision = (e: any, id: number, name:string, phone: number)  =>  {
        e.preventDefault();
        this.props.actions.modalEditApproveDivision(id, name, phone);
    }

    render() {
        const {isOpenEditDivision} = this.props;

        const {id, name, phone} = this.props.division;

        if(!isOpenEditDivision) return null;
        return (
            <div className="modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Редактирование записи</h5>
                            <button onClick={this.props.actions.closeModalEditDivision} type="button" className="close">
                            <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="item-add-form d-flex flex-column">
                                <label>
                                    Name:
                                    <input type="text"
                                        placeholder={name}
                                        className="form-control"
                                        required
                                        onChange={this.nameChange}
                                    />
                                </label>
                                <label>
                                    Phone:
                                    <input type="text"
                                        placeholder={phone}
                                        className="form-control"
                                        required
                                        onChange={this.phoneChange}
                                    />
                                </label>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary"
                                onClick={(e: any) =>
                                    this.modalEditApproveDivision(
                                        e,
                                        id,
                                        this.state.name,
                                        this.state.phone,
                                    )
                                }>
                                Подтвердить
                            </button>
                            <button
                                onClick={this.props.actions.closeModalEditDivision} type="button"
                                className="btn btn-secondary">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStoreStateModalEditDivision {
    return {
        isOpenEditDivision: state.reducerModalEditDivision.isOpenEditDivision,
        division: state.reducerModalEditDivision.division
    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    }
}

const connectModalEditDivision = connect(mapStateToProps,mapDispatchToProps)(ModalEditDivision);
export {connectModalEditDivision as ModalEditDivision}
