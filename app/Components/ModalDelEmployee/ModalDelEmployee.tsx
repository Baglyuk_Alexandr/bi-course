import * as React from 'react';
import { IDispatch, IStoreStateModalDelEmployee } from '../../Interfaces/Interfaces';
import { IStoreAll } from '../../Actions/Models';
import { IActionType } from '../../common';
import { Actions } from '../../Actions/Actions';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import './ModalDelEmployee.less';

type IModalDelEmployee = IStoreStateModalDelEmployee & IDispatch & any

class ModalDelEmployee extends React.Component<IModalDelEmployee> {

    modalDelApproveEmployee = (e: any, id: number)  =>  {
        e.preventDefault();
        this.props.actions.modalDelApproveEmployee(id);
    }

    render() {
        const {isOpenDelEmployee, id} = this.props;

        if(!isOpenDelEmployee) return null;
        return (
            <div className="modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Подтверждение удаления</h5>
                            <button onClick={this.props.actions.closeModalDelEmployee} type="button" className="close">
                                <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>Вы уверены что хотите удалить запись?</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary"
                                onClick={(e: any) => this.modalDelApproveEmployee(e, id)}>
                                Подтвердить
                            </button>
                            <button
                                onClick={this.props.actions.closeModalDelEmployee} type="button"
                                className="btn btn-secondary">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStoreStateModalDelEmployee {
    return {
        isOpenDelEmployee: state.reducerModalDelEmployee.isOpenDelEmployee,
        id: state.reducerModalDelEmployee.id
    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    }
}

const connectModalDelEmployee = connect(mapStateToProps,mapDispatchToProps)(ModalDelEmployee);
export {connectModalDelEmployee as ModalDelEmployee}
