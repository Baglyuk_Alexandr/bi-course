import * as React from 'react';
import { Dispatch } from 'redux';
import './Divisions.less';
import { IActionType } from '../../common';
import { connect } from 'react-redux';
import { Actions } from '../../Actions/Actions';
import { IStoreAll } from '../../Actions/Models';
import { IStateDivision, IDispatch, IStateListDivision, IPropsMatch } from '../../Interfaces/Interfaces';
import { Link } from 'react-router-dom';

type IDiv = IStateListDivision & IDispatch & IPropsMatch

class Division extends React.Component<IDiv> {

    componentDidMount() {
        const id = this.props.match.params.id; // id from url
        this.props.actions.initDivData(id);
    }

    openModalDelDivision = (e: any, id: number)  =>  {
        e.preventDefault();
        this.props.actions.openModalDelDivision(id);
    }

    openModalEditDivision = (e: any, id: number, name: string, phone: number)  =>  {
        e.preventDefault();
        this.props.actions.openModalEditDivision(id, name, phone);
    }

    render() {
        // if(this.props.division) return <h1>Список пуст</h1>;
        const division = this.props.division.map((item: IStateDivision) => {
            const {id, name, phone} = item;
            return (
                <div className="d-tr" key={id}>
                    <div className="d-td">{id}</div>
                    <div className="d-td">{name}</div>
                    <div className="d-td">{phone}</div>
                    <div className="d-td">
                        <div className="b-container">
                            <button
                                onClick={(e: any) => this.openModalEditDivision(e, id, name, phone)}>
                                    Изменить
                            </button>
                            <button
                                onClick={(e: any) => this.openModalDelDivision(e, id)}>
                                    Удалить
                            </button>
                        </div>
                    </div>
                    <div className="d-td">
                        <div className="b-container">
                            <button>
                                <Link className="link" to={`/employee/${id}`}>Работники</Link>
                            </button>
                        </div>
                    </div>
                </div>
            )
        })
        return (
            <div className="d-container">
                <h1>Подразделения</h1>
                <div className="innerNav">
                    <Link className="f-right"to="/organizations">← Организации</Link>
                </div>
                <div>
                    <button className="addItem"
                        onClick={this.props.actions.openModalAddOrganization}>
                            Добавить запись
                    </button>
                </div>
                <div className="d-table">
                    <div className="d-tr">
                        <div className="d-td">ID</div>
                        <div className="d-td">Name</div>
                        <div className="d-td">Phone</div>
                        <div className="d-td">Controls</div>
                        <div className="d-td">Entry</div>
                    </div>
                    {division.length > 0? <>{division}</>:<h1>Список пуст</h1>}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStateListDivision {
    return {
        division: state.reducerDivision.division
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    };
}

const connectDivision = connect(mapStateToProps, mapDispatchToProps)(Division);

export {connectDivision as Division};
