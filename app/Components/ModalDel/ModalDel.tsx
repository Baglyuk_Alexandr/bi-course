import * as React from 'react';
import { IDispatch, IStoreStateModalDel } from '../../Interfaces/Interfaces';
import { IStoreAll } from '../../Actions/Models';
import { IActionType } from '../../common';
import { Actions } from '../../Actions/Actions';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import './ModalDel.less';

type IModalDel = IStoreStateModalDel & IDispatch & any

class ModalDel extends React.Component<IModalDel> {

    modalDelApprove = (e: any, id: number)  =>  {
        e.preventDefault();
        this.props.actions.modalDelApprove(id);
    }

    render() {
        const {isOpenDel, id} = this.props;

        if(!isOpenDel) return null;
        return (
            <div className="modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Подтверждение удаления</h5>
                            <button onClick={this.props.actions.closeModalDel} type="button" className="close">
                                <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>Вы уверены что хотите удалить запись?</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary"
                                onClick={(e: any) => this.modalDelApprove(e, id)}>
                                Подтвердить
                            </button>
                            <button
                                onClick={this.props.actions.closeModalDel} type="button"
                                className="btn btn-secondary">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStoreStateModalDel {
    return {
        isOpenDel: state.reducerModalDel.isOpenDel,
        id: state.reducerModalDel.id
    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    }
}

const connectModalDel = connect(mapStateToProps,mapDispatchToProps)(ModalDel);
export {connectModalDel as ModalDel}
