import * as React from 'react';
import { Dispatch } from 'redux';
import './Organizations.less';
import { IActionType } from '../../common';
import { connect } from 'react-redux';
import { Actions } from '../../Actions/Actions';
import { IStoreAll } from '../../Actions/Models';
import { IStateOrganization, IDispatch, IStateListOrganizations, IStateOrganizations } from '../../Interfaces/Interfaces';
import { Link } from 'react-router-dom';

/**
 * Пропсы компонента из стора.
 * @prop {boolean} loginStatus Состояние зарегистрированности пользователя.
 */

type IOrg = IStateListOrganizations & IDispatch

class Organizations extends React.Component<IOrg> {

    componentDidMount() {
        this.props.actions.initOrgData()
    }

    openModalDelOrganization = (e: any, id: number)  =>  {
        e.preventDefault();
        this.props.actions.openModalDelOrganization(id);
    }

    openModalEditOrganization = (e: any, id: number, name: string, address: string, INN: number)  =>  {
        e.preventDefault();
        this.props.actions.openModalEditOrganization(id, name, address, INN);
    }

    // openModalAddOrganization = (e: any, id: number, name: string, address: string, INN: number)  =>  {
    //     e.preventDefault();
    //     this.props.actions.openModalAddOrganization(id, name, address, INN);
    // }

    render() {
        const organizations = this.props.organizations.map((organization: IStateOrganization) => {
            const {id, name, address, INN} = organization;
            return (
                <div className="d-tr" key={id}>
                    <div className="d-td">{id}</div>
                    <div className="d-td">{name}</div>
                    <div className="d-td">{address}</div>
                    <div className="d-td">{INN}</div>
                    <div className="d-td">
                        <div className="b-container">
                            <button
                                onClick={(e: any) => this.openModalEditOrganization(e, id, name, address, INN)}>
                                    Изменить
                            </button>
                            <button
                                onClick={(e: any) => this.openModalDelOrganization(e, id)}>
                                    Удалить
                            </button>
                        </div>
                    </div>
                    <div className="d-td">
                        <div className="b-container">
                            <button>
                                <Link className="link" to={`/division/${id}`}>Подразделения</Link>
                            </button>
                        </div>
                    </div>
                </div>
            )
        })
        return (
            <div className="d-container">
                <h1>Организации</h1>
                <div>
                    <button className="addItem"
                        onClick={this.props.actions.openModalAddOrganization}>
                            Добавить запись
                    </button>
                </div>
                <div className="d-table">
                    <div className="d-tr">
                        <div className="d-td">ID</div>
                        <div className="d-td">Name</div>
                        <div className="d-td">Adress</div>
                        <div className="d-td">INN</div>
                        <div className="d-td">Controls</div>
                        <div className="d-td">Entry</div>
                    </div>
                    {organizations.length > 0? <>{organizations}</>:<h1>Список пуст</h1>}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStateOrganizations {
    return {
        loginStatus: state.reducerAuthForm.loginStatus,
        organizations: state.reducerOrganizations.organizations
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    };
}

const connectOrganizations = connect(mapStateToProps, mapDispatchToProps)(Organizations);

export {connectOrganizations as Organizations};
