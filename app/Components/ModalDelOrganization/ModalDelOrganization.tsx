import * as React from 'react';
import { IDispatch, IStoreStateModalDelOrganization } from '../../Interfaces/Interfaces';
import { IStoreAll } from '../../Actions/Models';
import { IActionType } from '../../common';
import { Actions } from '../../Actions/Actions';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import './ModalDelOrganization.less';

type IModalDelOrganization = IStoreStateModalDelOrganization & IDispatch & any

class ModalDelOrganization extends React.Component<IModalDelOrganization> {

    modalDelApproveOrganization = (e: any, id: number)  =>  {
        e.preventDefault();
        this.props.actions.modalDelApproveOrganization(id);
    }

    render() {
        const {isOpenDelOrganization, id} = this.props;

        if(!isOpenDelOrganization) return null;
        return (
            <div className="modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Подтверждение удаления</h5>
                            <button onClick={this.props.actions.closeModalDelOrganization} type="button" className="close">
                                <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>Вы уверены что хотите удалить запись?</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary"
                                onClick={(e: any) => this.modalDelApproveOrganization(e, id)}>
                                Подтвердить
                            </button>
                            <button
                                onClick={this.props.actions.closeModalDelOrganization} type="button"
                                className="btn btn-secondary">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStoreStateModalDelOrganization {
    return {
        isOpenDelOrganization: state.reducerModalDelOrganization.isOpenDelOrganization,
        id: state.reducerModalDelOrganization.id
    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    }
}

const connectModalDelOrganization = connect(mapStateToProps,mapDispatchToProps)(ModalDelOrganization);
export {connectModalDelOrganization as ModalDelOrganization}
