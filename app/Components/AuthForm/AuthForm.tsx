import * as React from 'react';
import './AuthForm.less';
import { Actions } from '../../Actions/Actions';
import { Dispatch } from 'redux';
import { IActionType } from '../../common';
import { connect } from 'react-redux';
import { IStoreAll } from '../../Actions/Models';
import { IStateProps, IDispatch } from '../../Interfaces/Interfaces';

/**
 * Пропсы компонента из стора.
 * @prop {boolean} loginStatus Состояние зарегистрированности пользователя.
 * @prop {boolean} waitingForLogin Ожидание завершения процедуры авторизации (завершение логина).
 */

type IAuth = IStateProps & IDispatch

class AuthForm extends React.Component<IAuth> {

    state = {
        login: '',
        password:''
    };

    loginChange = (e: any) => {
        this.setState({
           login:e.target.value
       });
   };

   passwordChange = (e: any) => {
        this.setState({
            password:e.target.value
        });
        console.log(this.state.password)
    };

    handleSubmit(e: any) {
        e.preventDefault()
        this.props.actions.onLogin({login: this.state.login, password: this.state.password})
    }

    render() {
        const {loginStatus,waitingForLogin} = this.props;
        return (
            <div className="authorizeForm">
                <form onSubmit={(e) => this.handleSubmit(e)} className="item-add-form d-flex flex-column">
                    <label>
                        Login:
                        <input type="text"
                            placeholder="Enter you Login"
                            className="form-control"
                            required
                            onChange={this.loginChange}
                            value={this.state.login}
                            />
                    </label>
                    <label>
                        Password:
                        <input type="password"
                            placeholder="Enter you password"
                            className="form-control"
                            required
                            onChange={this.passwordChange}
                            value={this.state.password}
                            />
                    </label>
                    <button type="submit"
                        className="btn btn-outline-secondary">
                            Apply
                    </button>
                </form>
                <div className="authorize">
                    <p>Состояние авторизации:</p>
                    {
                        waitingForLogin ?
                            <p>Авторизация...</p> :
                            loginStatus ?
                                <p>
                                    Login success
                                </p> :
                                <p>
                                    Logged out
                                </p>
                    }
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStateProps {
    return {
        loginStatus: state.reducerAuthForm.loginStatus,
        waitingForLogin: state.reducerAuthForm.loading
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    };
}

const connectAuthForm = connect(mapStateToProps, mapDispatchToProps)(AuthForm);

export {connectAuthForm as AuthForm};
