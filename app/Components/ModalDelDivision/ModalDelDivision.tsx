import * as React from 'react';
import { IDispatch, IStoreStateModalDelDivision } from '../../Interfaces/Interfaces';
import { IStoreAll } from '../../Actions/Models';
import { IActionType } from '../../common';
import { Actions } from '../../Actions/Actions';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import './ModalDelDivision.less';

type IModalDelDivision = IStoreStateModalDelDivision & IDispatch & any

class ModalDelDivision extends React.Component<IModalDelDivision> {

    modalDelApproveDivision = (e: any, id: number)  =>  {
        e.preventDefault();
        this.props.actions.modalDelApproveDivision(id);
    }

    render() {
        const {isOpenDelDivision, id} = this.props;

        if(!isOpenDelDivision) return null;
        return (
            <div className="modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Подтверждение удаления</h5>
                            <button onClick={this.props.actions.closeModalDelDivision} type="button" className="close">
                                <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>Вы уверены что хотите удалить запись?</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary"
                                onClick={(e: any) => this.modalDelApproveDivision(e, id)}>
                                Подтвердить
                            </button>
                            <button
                                onClick={this.props.actions.closeModalDelDivision} type="button"
                                className="btn btn-secondary">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStoreStateModalDelDivision {
    return {
        isOpenDelDivision: state.reducerModalDelDivision.isOpenDelDivision,
        id: state.reducerModalDelDivision.id
    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    }
}

const connectModalDelDivision = connect(mapStateToProps,mapDispatchToProps)(ModalDelDivision);
export {connectModalDelDivision as ModalDelDivision}
