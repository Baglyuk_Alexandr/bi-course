import * as React from 'react';
import { Actions } from '../../Actions/Actions';
import { IStoreAll } from '../../Actions/Models';
import { Dispatch } from 'redux';
import { IActionType } from '../../common';
import { IDispatch, IStoreStateModalAddEmployee } from '../../Interfaces/Interfaces';
import { connect } from 'react-redux';
import './ModalAddEmployee.less';

type IModaAddEmployee = IStoreStateModalAddEmployee & IDispatch & any

class ModalAddEmployee extends React.Component<IModaAddEmployee> {

    state = {
        id: 100,
        name: '',
        address: '',
        INN: 0
    }

    idChange = (e: any) => {
        this.setState({
            id: e.target.value
        });
    }

    nameChange = (e: any) => {
        this.setState({
            name: e.target.value
        });
    }

    addressChange = (e: any) => {
        this.setState({
            address: e.target.value
        });
    }

    innChange = (e: any) => {
        this.setState({
            INN: e.target.value
        });
    }

    modalAddApproveEmployee = (e: any, id: number, name: string, address: string, INN: number)  =>  {
        e.preventDefault();
        this.props.actions.modalAddApproveEmployee(id, name, address, INN);
    }

    render() {
        const {isOpenAddEmployee} = this.props;

        if(!isOpenAddEmployee) return null;
        return (
            <div className="modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Редактирование записи</h5>
                            <button onClick={this.props.actions.closeModalAddEmployee} type="button" className="close">
                            <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="item-add-form d-flex flex-column">
                                <label>
                                    ID:
                                    <input type="text"
                                        placeholder='Введите ID'
                                        className="form-control"
                                        required
                                        onChange={this.idChange}
                                    />
                                </label>
                                <label>
                                    Name:
                                    <input type="text"
                                        placeholder='Введите название'
                                        className="form-control"
                                        required
                                        onChange={this.nameChange}
                                    />
                                </label>
                                <label>
                                    Address:
                                    <input type="text"
                                        placeholder='Введите адрес'
                                        className="form-control"
                                        required
                                        onChange={this.addressChange}
                                    />
                                </label>
                                <label>
                                    INN:
                                    <input type="text"
                                        placeholder='Введите ИНН'
                                        className="form-control"
                                        required
                                        onChange={this.innChange}
                                    />
                                </label>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary"
                                onClick={(e: any) =>
                                    this.modalAddApproveEmployee(
                                        e,
                                        this.state.id,
                                        this.state.name,
                                        this.state.address,
                                        this.state.INN
                                    )
                                }>
                                Подтвердить
                            </button>
                            <button
                                onClick={this.props.actions.closeModalAddEmployee} type="button"
                                className="btn btn-secondary">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStoreStateModalAddEmployee {
    return {
        isOpenAddEmployee: state.reducerModalAddEmployee.isOpenAddEmployee,
        employee: state.reducerModalAddEmployee.employee
    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    }
}

const connectModalAddEmployee = connect(mapStateToProps,mapDispatchToProps)(ModalAddEmployee);
export {connectModalAddEmployee as ModalAddEmployee}
