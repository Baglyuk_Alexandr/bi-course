import * as React from 'react';
import { Actions } from '../../Actions/Actions';
import { IStoreAll } from '../../Actions/Models';
import { Dispatch } from 'redux';
import { IActionType } from '../../common';
import { IDispatch, IStoreStateModalEditOrganization } from '../../Interfaces/Interfaces';
import { connect } from 'react-redux';
import './ModalEditOrganization.less';

type IModalDelOrganization = IStoreStateModalEditOrganization & IDispatch & any

class ModalEditOrganization extends React.Component<IModalDelOrganization> {

    state = {
        name: '',
        address: '',
        INN: 0
    }

    nameChange = (e: any) => {
        this.setState({
            name: e.target.value
        });
    }

    addressChange = (e: any) => {
        this.setState({
            address: e.target.value
        });
    }

    innChange = (e: any) => {
        this.setState({
            INN: e.target.value
        });
    }

    modalEditApproveOrganization = (e: any, id: number, name: string, address: string, INN: number)  =>  {
        e.preventDefault();
        this.props.actions.modalEditApproveOrganization(id, name, address, INN);
    }

    render() {
        const {isOpenEditOrganization} = this.props;

        const {id, name, address, INN} = this.props.organization;

        if(!isOpenEditOrganization) return null;
        return (
            <div className="modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Редактирование записи</h5>
                            <button onClick={this.props.actions.closeModalEditOrganization} type="button" className="close">
                            <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="item-add-form d-flex flex-column">
                                <label>
                                    Name:
                                    <input type="text"
                                        placeholder={name}
                                        className="form-control"
                                        required
                                        onChange={this.nameChange}
                                    />
                                </label>
                                <label>
                                    Address:
                                    <input type="text"
                                        placeholder={address}
                                        className="form-control"
                                        required
                                        onChange={this.addressChange}
                                    />
                                </label>
                                <label>
                                    INN:
                                    <input type="text"
                                        placeholder={INN}
                                        className="form-control"
                                        required
                                        onChange={this.innChange}
                                    />
                                </label>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary"
                                onClick={(e: any) =>
                                    this.modalEditApproveOrganization(
                                        e,
                                        id,
                                        this.state.name,
                                        this.state.address,
                                        this.state.INN
                                    )
                                }>
                                Подтвердить
                            </button>
                            <button
                                onClick={this.props.actions.closeModalEditOrganization} type="button"
                                className="btn btn-secondary">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStoreStateModalEditOrganization {
    return {
        isOpenEditOrganization: state.reducerModalEditOrganization.isOpenEditOrganization,
        organization: state.reducerModalEditOrganization.organization
    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    }
}

const connectModalEditOrganization = connect(mapStateToProps,mapDispatchToProps)(ModalEditOrganization);
export {connectModalEditOrganization as ModalEditOrganization}
