import * as React from 'react';
import { Actions } from '../../Actions/Actions';
import { IStoreAll } from '../../Actions/Models';
import { Dispatch } from 'redux';
import { IActionType } from '../../common';
import { IDispatch, IStoreStateModalEditEmployee } from '../../Interfaces/Interfaces';
import { connect } from 'react-redux';
import './ModalEditEmployee.less';

type IModalDelEmployee = IStoreStateModalEditEmployee & IDispatch & any

class ModalEditEmployee extends React.Component<IModalDelEmployee> {

    state = {
        FIO: '',
        address: '',
        vacansiya: ''
    }

    fioChange = (e: any) => {
        this.setState({
            FIO: e.target.value
        });
    }

    addressChange = (e: any) => {
        this.setState({
            address: e.target.value
        });
    }

    vakansiyaChange = (e: any) => {
        this.setState({
            vacansiya: e.target.value
        });
    }

    modalEditApproveEmployee = (e: any, id: number, FIO: string, address: string, vacansiya: string)  =>  {
        e.preventDefault();
        this.props.actions.modalEditApproveEmployee(id, FIO, address, vacansiya);
    }

    render() {
        const {isOpenEditEmployee} = this.props;

        const {id, FIO, address, position} = this.props.employee;

        if(!isOpenEditEmployee) return null;
        return (
            <div className="modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Редактирование записи</h5>
                            <button onClick={this.props.actions.closeModalEditEmployee} type="button" className="close">
                            <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="item-add-form d-flex flex-column">
                                <label>
                                    FIO:
                                    <input type="text"
                                        placeholder={FIO}
                                        className="form-control"
                                        required
                                        onChange={this.fioChange}
                                    />
                                </label>
                                <label>
                                    Adres:
                                    <input type="text"
                                        placeholder={address}
                                        className="form-control"
                                        required
                                        onChange={this.addressChange}
                                    />
                                </label>
                                <label>
                                    Vacansiya:
                                    <input type="text"
                                        placeholder={position}
                                        className="form-control"
                                        required
                                        onChange={this.vakansiyaChange}
                                    />
                                </label>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary"
                                onClick={(e: any) =>
                                    this.modalEditApproveEmployee(
                                        e,
                                        id,
                                        this.state.FIO,
                                        this.state.address,
                                        this.state.vacansiya
                                    )
                                }>
                                Подтвердить
                            </button>
                            <button
                                onClick={this.props.actions.closeModalEditEmployee} type="button"
                                className="btn btn-secondary">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStoreStateModalEditEmployee {
    return {
        isOpenEditEmployee: state.reducerModalEditEmployee.isOpenEditEmployee,
        employee: state.reducerModalEditEmployee.employee
    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    }
}

const connectModalEditEmployee = connect(mapStateToProps,mapDispatchToProps)(ModalEditEmployee);
export {connectModalEditEmployee as ModalEditEmployee}
