import * as React from 'react';
import { Actions } from '../../Actions/Actions';
import { IStoreAll } from '../../Actions/Models';
import { Dispatch } from 'redux';
import { IActionType } from '../../common';
import { IDispatch, IStoreStateModalAddDivision } from '../../Interfaces/Interfaces';
import { connect } from 'react-redux';
import './ModalAddDivision.less';

type IModaAddDivision = IStoreStateModalAddDivision & IDispatch & any

class ModalAddDivision extends React.Component<IModaAddDivision> {

    state = {
        id: 100,
        name: '',
        phone: 911,
    }

    idChange = (e: any) => {
        this.setState({
            id: e.target.value
        });
    }

    nameChange = (e: any) => {
        this.setState({
            name: e.target.value
        });
    }

    phoneChange = (e: any) => {
        this.setState({
            phone: e.target.value
        });
    }

    modalAddApproveDivision = (e: any, id: number, name: string, phone: number)  =>  {
        e.preventDefault();
        this.props.actions.modalAddApproveDivision(id, name, phone);
    }

    render() {
        const {isOpenAddDivision} = this.props;

        if(!isOpenAddDivision) return null;
        return (
            <div className="modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Редактирование записи</h5>
                            <button onClick={this.props.actions.closeModalAddDivision} type="button" className="close">
                            <span>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="item-add-form d-flex flex-column">
                                <label>
                                    ID:
                                    <input type="text"
                                        placeholder='Введите ID'
                                        className="form-control"
                                        required
                                        onChange={this.idChange}
                                    />
                                </label>
                                <label>
                                    Название:
                                    <input type="text"
                                        placeholder='Введите название'
                                        className="form-control"
                                        required
                                        onChange={this.nameChange}
                                    />
                                </label>
                                <label>
                                    Телефон:
                                    <input type="text"
                                        placeholder='Введите адрес'
                                        className="form-control"
                                        required
                                        onChange={this.phoneChange}
                                    />
                                </label>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary"
                                onClick={(e: any) =>
                                    this.modalAddApproveDivision(
                                        e,
                                        this.state.id,
                                        this.state.name,
                                        this.state.phone
                                    )
                                }>
                                Подтвердить
                            </button>
                            <button
                                onClick={this.props.actions.closeModalAddDivision} type="button"
                                className="btn btn-secondary">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: IStoreAll): IStoreStateModalAddDivision {
    return {
        isOpenAddDivision: state.reducerModalAddDivision.isOpenAddDivision,
        division: state.reducerModalAddDivision.division
    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
    return {
        actions: new Actions(dispatch)
    }
}

const connectModalAddDivision = connect(mapStateToProps,mapDispatchToProps)(ModalAddDivision);
export {connectModalAddDivision as ModalAddDivision}
