import * as React from 'react';
import {Link} from 'react-router-dom';
import './Navbar.less';

interface IPropse {
    loginStatus: boolean;
    waitingForLogin: boolean;
}

export default class NavBar extends React.Component<IPropse> {
    render() {
        return (
            <nav className="navigation">
                <ul>
                    {this.props.waitingForLogin}
                    <li>
                        <Link to="/organizations">Organizations</Link>
                    </li>
                    {
                        // LogOut не реализована на сервере
                        this.props.loginStatus ?
                            <li>
                                <Link to="/">LogOut</Link>
                            </li> :
                            <li>
                                <Link to="/">LogIn</Link>
                            </li>
                    }
                </ul>
            </nav>
        )
    }
}
